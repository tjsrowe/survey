FROM node:latest

RUN mkdir /survey
WORKDIR /survey

COPY app.js /survey/app.js
COPY survey.js /survey/survey.js
COPY package.json /survey/package.json
COPY package-lock.json /survey/package-lock.json

RUN mkdir /survey/images
COPY images/* /survey/images/

RUN npm config set proxy "http://i7dev.home.tjsr.id.au:8482/"
RUN npm config set strict-ssl false
RUN npm install -g sortablejs --save
RUN npm install

EXPOSE 3007

COPY index.html /survey/index.html
COPY submit.html /survey/submit.html
COPY style.css /survey/style.css
COPY config.json /survey/config.json

CMD ["node", "app.js"]