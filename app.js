const survey = require('./survey');

const http = require('http');
const handler = require('serve-handler');

var getIP = require('ipware')().get_ip;

const hostname = '0.0.0.0';
//const port = 3007;

function onRequest(request, response) {
	try {
		var ipInfo = getIP(request);
		var clientIp = ipInfo.clientIp;

		survey.serveRequest(clientIp, request, response);
	} catch (exception) {
		console.log("Exception while serving index: " + exception.stack);
		response.end();
	}
}

const server = http.createServer(onRequest);

survey.init(function(port) {
	console.log("Initializing on port " + port);
	server.listen(port, hostname, () => {
		console.log(`Server running at http://${hostname}:${port}/`);
	});
});

