'use strict';

var fs = require('fs');
var request = require('request');
var getIP = require('ipware')().get_ip;
var url = require('url');
var md5 = require('md5');
var querystring = require('querystring');

const ALLOWED_RESOURCES = [ '/favicon.ico', '/index.html', '/style.css', '/images/*.jpg', '/images/*/*.jpg' ];
const DEFAULT_CONFIG_FILE = 'config.json';
const DEFAULT_PORT = 3007;
var config = { "port": DEFAULT_PORT };

// Complete SortableJS (with all plugins)
const Sortable = require('sortablejs');

exports.readConfig = async function (fileToRead, onConfigRead) {
	if (!fileToRead) {
		fileToRead = DEFAULT_CONFIG_FILE;
	}
	fs.readFile(fileToRead, 'utf8', function (err, data) {
		console.log("Config read from " + fileToRead);
		config = JSON.parse(data);
		exports.config = config;
		console.log("Config loaded from " + fileToRead + ": " + JSON.stringify(config));
		onConfigRead(config.port);
		return config;
	});
}

function matchRuleShort(str, rule) {
  var escapeRegex = (str) => str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
  return new RegExp("^" + rule.split("*").map(escapeRegex).join(".*") + "$").test(str);
}


function allowedResource(url) {
	for (var i = 0;i < ALLOWED_RESOURCES.length;i++) {
		if (strcasecmp(ALLOWED_RESOURCES[i], url) || matchRuleShort(url, ALLOWED_RESOURCES[i])) {
			return true;
		}
	}
	return false;
}

function logRequest(clientIp, url, responseCode) {
	console.log(clientIp + " " + url + " " + responseCode);
}

function saveData(request) {
	// Write data to a new file using the current timestamp.
	
	var d = new Date();
	var timestamp = d.getTime();
	
	var fs = require('fs');
	var dir = config.dataDir;

	if (!fs.existsSync(dir)){
		fs.mkdirSync(dir);
	}
	var fileToWrite = dir + "/" + timestamp + ".json";
	var submitData;
	if (request.post.data) {
		submitData = unescape(request.post.data);
	} else {
		submitData = JSON.stringify(request.post);
	}
	console.log("Response received: " + submitData);
	
	fs.writeFileSync(fileToWrite, submitData, 'utf8', function(err) {
		if (err) {
			errCallback(err);
			ok = false;
		}
	});
//	if (ok) {
//		successCallback();
//	}
}

function writeDataSubmittedHeader(response) {
	fs.readFile('./submit.html', function(err, data) {
		response.writeHead(200, 'OK');
		response.end(data);
	});
}

function processPost(request, response, callback) {
    var queryData = "";
    if(typeof callback !== 'function') return null;

    if (strcasecmp(request.method, 'POST')) {
        request.on('data', function(data) {
            queryData += data;
            if(queryData.length > 1e6) {
                queryData = "";
                response.writeHead(413, {'Content-Type': 'text/plain'}).end();
                request.connection.destroy();
            }
        });

        request.on('end', function() {
            request.post = querystring.parse(queryData);
            callback(request);
			writeDataSubmittedHeader(response);
        });
    } else {
		console.log(request.method);
        response.writeHead(405, {'Content-Type': 'text/plain'});
        response.end();
    }
}

exports.submit = function(request, response) {
	processPost(request, response, saveData);
};

function getSurveyDataForId(id) {
	var len = config.surveys.length;
	for (var cur = 0;cur < len;cur++) {
		var survey = config.surveys[cur];
		if (survey.id == id) {
			return survey;
		}
	}
	
	return null;
}

exports.getSurveyStruct = function(request, response) {
	var par = params(request);
	console.log(request.url);
	var urlParts = request.url.split("/");
	var surveyId = urlParts[2];
	console.log("Searching for id " + surveyId);
	
	var survey = getSurveyDataForId(surveyId);
	console.log(JSON.stringify(survey));
	response.end(JSON.stringify(survey));
};

function validateId(request) {
	var par = params(request);
	console.log(request.url);
	var urlParts = request.url.split("/");
	var surveyId = urlParts[1];
	console.log("Searching for id " + surveyId);
	
	var survey = getSurveyDataForId(surveyId);
	if (survey) {
		return true;
	}
	return false;
}

exports.serveRequest = function (clientIp, request, response) {
	var path = url.parse(request.url).pathname;
	if (request.url.startsWith('/submit')) {
		try {
			exports.submit(request, response);
		} catch (exception) {
			console.log("Exception while parsing posted data: " + exception.stack);
			response.end();
		}
	}
	else if (request.url.startsWith('/survey')) {
		try {
			exports.getSurveyStruct(request, response);
		} catch (exception) {
			console.log("Exception while parsing posted data: " + exception.stack);
			response.end();
		}
	} else if (allowedResource(request.url) && fs.existsSync('.' + request.url)) {
		fs.readFile('.' + request.url, function(err, data) {
			response.end(data);
		});
		logRequest(clientIp, request.url, 200);
	} else if (!strcasecmp(path, "/")) {
		if (!response) {
			console.log("No response object.");
		} else if (validateId(request)) {
			fs.readFile('./index.html', function(err, data) {
				response.writeHead(200, 'OK');
				response.end(data);
			});
			logRequest(clientIp, request.url, 200);
		} else {
			//findSurveyForId(request, response);
			response.writeHead(404, 'Not found');
			response.end('Not found');
			logRequest(clientIp, request.url, 404);
		}
	} else {
		fs.readFile('./index.html', function(err, data) {
			response.writeHead(200, 'OK');
			response.end(data);
		});
		logRequest(clientIp, request.url, 200);
	}
}; 

function strcasecmp(string1, string2) {
	if (string1 == null && string2 == null) {
		return true;
	}
	if (string1 == null || string2 == null) {
		return false;
	}
	string1 = string1.toLowerCase();
	string2 = string2.toLowerCase();
	
	return string1 === string2;
}

exports.setOptions = function(options) {
	if (options && options.port) {
		config.port = options.port;
	}
};


exports.init = async function(onComplete, configToRead, options) {
	try {
		exports.setOptions(options);
		
		await exports.readConfig(configToRead, onComplete);
	} catch (e) {
		console.log("Exception while reading config " + configToRead);
		console.log(e);
	}
	//onComplete();
};

var params = function (req) {
  let q=req.url.split('?'),result={};
  if(q.length>=2){
      q[1].split('&').forEach((item)=>{
           try {
             result[item.split('=')[0]]=item.split('=')[1];
           } catch (e) {
             result[item.split('=')[0]]='';
           }
      })
  }
  return result;
}
